# Guide d'installation - Projet DevSecOps
<img src="./public/assets/DevSecOps.png" >
## Étape 1 : Création de la machine virtuelle

Commencez par créer une machine virtuelle en utilisant la configuration recommandée pour votre environnement.

## Étape 2 : Configuration des outils

Installez les outils nécessaires sur votre machine virtuelle :
- Docker
- Jenkins
- Trivy
- SonarQube

installer les plugins requis pour Jenkins :
- Docker
- Eclipse Temurin Installer
- SonarQube Scanner
- NodeJs Plugin
- Email Extension Plugin
- OWASP Dependency-Check
- Prometheus

## Étape 3 : Création de tokens et configurations

1. Créez un token dans SonarQube et enregistrez-le dans les informations d'identification de Jenkins.
2. Configurez un webhook dans SonarQube pour Jenkins.

## Étape 4 : Création du projet Jenkins

Créez un nouveau projet Jenkins en utilisant un pipeline.

## Étape 5 : Configuration du pipeline Jenkins

Utilisez le code ci-dessous pour configurer le pipeline Jenkins :
 
pipeline{
    agent any
    tools{
        jdk 'jdk17'
        nodejs 'node16'
    }
    environment {
        SCANNER_HOME=tool 'sonar-scanner'
    }
    stages {
        stage('clean workspace'){
            steps{
                cleanWs()
            }
        }
        // ... (voir le reste du code fourni dans fichier pipeline.txt)
    }
} 

<div align="center">
  <img src="./images/jenkins.PNG" alt="Logo" width="100%" height="100%">

  <br>
    <img src="./images/sonarqube.PNG" >
</div>
## Étape  6: Implémentation des notifications par e-mail dans Jenkins

Configurez Jenkins pour envoyer des notifications par e-mail en cas de succès ou d'échec du pipeline'.
1. Connectez-vous à votre instance Jenkins.

2. Allez dans "Manage Jenkins" -> "Configure System".

3. Sous la section "E-mail Notification", configurez les paramètres SMTP de votre serveur de messagerie.

4. Sous la section "Extended E-mail Notification", configurez les options pour les notifications avancées. 
<img src="./images/email.PNG" >
## Étape 7 : Configuration de la machine de surveillance

1. Créez une deuxième machine virtuelle pour la surveillance.
2. Installez Prometheus et Grafana sur cette machine.
3. Configurez le node exporter sur la machine virtuelle en suivant les instructions [ici](https://github.com/prometheus/node_exporter).
4. Modifiez le fichier de configuration de Prometheus (`prometheus.yml`) pour inclure le node exporter et Jenkins comme cibles à surveiller. Voici un exemple :


scrape_configs:
  - job_name: 'node-exporter'
    static_configs:
      - targets: ['<node_exporter_ip>:9100']

  - job_name: 'jenkins'
    static_configs:
      - targets: ['<jenkins_ip>:8080']
.
<img src="./images/prometheus.PNG" >
<img src="./images/grafana jenkins.PNG" >
<img src="./images/grafana node.PNG" >


## Étape 8 : Création d'un cluster Kubernetes

1. Créez un cluster Kubernetes en suivant les instructions de votre fournisseur de services cloud ou en utilisant un outil comme Minikube.

2. Ajoutez le node exporter au cluster Kubernetes. Vous pouvez utiliser la configuration standard en suivant ces étapes :

   ```bash
   kubectl apply -f https://raw.githubusercontent.com/prometheus/node_exporter/master/deploy/kube/node-exporter-daemonset.yaml
## Étape 9 : Configuration de Prometheus pour Kubernetes

Modifiez le fichier de configuration de Prometheus (`prometheus.yml`) pour inclure le node exporter dans le cluster Kubernetes. Voici un exemple :


scrape_configs:
  - job_name: 'node-exporter'
    kubernetes_sd_configs:
      - role: node
## Étape 10 : Configuration d'ArgoCD dans Kubernetes

1. Installez ArgoCD dans le cluster Kubernetes en suivant le guide officiel [ici](https://argoproj.github.io/argo-cd/getting_started/installation/).

2. Configurez ArgoCD pour surveiller votre dépôt Git où se trouve votre application. Utilisez les commandes suivantes comme exemple :

bash
argocd repo add <repository_url>
argocd app create <app_name> --repo <repository_url> --path <path_to_app> --dest-namespace <namespace> --dest-server https://kubernetes.default.svc --sync-policy auto

<img src="./images/argocd.PNG" >

